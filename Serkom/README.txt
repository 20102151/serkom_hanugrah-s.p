Selamat datang di README Website "Beasiswa Pintar"

==================================================================================================================================================================================
Introduction :

Website "Beasiswa Pintar" merupakan platform untuk mahasiswa yang ingin mengikuti beasiswa dengan ketentuan tertentu. 
Website ini memberikan informasi terkait jenis beasiswa yang terseda di "Beasiswa Pintar". Mahasiswa yang memenuhi syarat dan
ingin mendaftarkan diri bisa melalui website ini.

==================================================================================================================================================================================
Menu :

1. Pilihan Beasiswa Page :
   Halaman ini merupakan dashboard dari website Beasiswa Pintar yang menampilkan landing page dan konten website. 
   Konten yang diberikan adalah card yang berisi informasi jenis beasiswa. Jika user ingin mendaftar beasiswa, 
   maka bisa langsung tekan button Daftar pada card. 

2. Daftar Page:
   Daftar Page merupakan halaman yang berisi formulir pendaftaran beasiswa. 
   User bisa melakukan pendaftaran beasiswa jika memiliki IPK >=3 dan diwajibkan untuk melengkapi formulir pendaftaran 
   serta mengunggah berkas persyaratan dalam bentuk file. Sebaliknya jika memiliki IPK <3, user akan tidak bisa melakukan
   pendaftaran di website, karena secara otomatis input jenis beasiswa, upload berkas, dan button daftar akan disable.

3. Hasil Page:
   Halaman ini menampilkan informasi tentang pendaftar beasiswa dalam bentuk tabel dan diagram batang

==================================================================================================================================================================================
Penggunaan Website

1. Melakukan Instalasi:
   - Pastikan Anda memiliki tools yang mendukung proses running PHP dan MySQL. ex : XAMPP
   - Buatlah database baru pada localhost Anda dan import tabel yang digunakan.

2. Konfigurasi Koneksi Database:
   - Buka file "koneksi.php".
   - Sesuaikan parameter koneksi database (host, username, password, "nama database") sesuai dengan konfigurasi pada server Anda.

3. Pilihan Beasiswa Page :
   - Buka file "index.php" pada browser dengan cara ketik localhost/nama_file/ untuk melihat halaman utama website.

4. Daftar Page:
   - Anda dapat melakukan klik menu "Daftar" pada navigasi bar untuk mengakses halaman pendaftaran.
   - Lakukan pengisian formulir pendaftaran sesuai ketentuan.
   - Unggah berkas yang diperlukan 
   - Klik button "Daftar" untuk melakukan submit formulir

5. Hasil Page:
   - Anda dapat melakukan klik pada menu "Hasil" di navigasi bar untuk melihat daftar pendaftar beasiswa 
   dalam bentuk tabel dan diagram batang

================================================================================================================================================================================
Lisensi:

Website ini dikembangan oleh Hanugrah Surya Purwaka (hanugrahsurya@gmail.com)
Tujuan dari pembuatan website ini adalah sebagai penugasan sertifikasi komputer Junior Web Development.

==================================================================================================================================================================================
