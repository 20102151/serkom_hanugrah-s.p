<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Melakukan pengaturan set endcoding menjadi UTF-8 -->
  <meta charset="UTF-8">
  <!-- untuk konfigurasi viewport agar desain lebih responsif -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--CDN CSS-->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  <!-- Penghubung link ke style CSS -->
  <link rel="stylesheet" type="text/css" href="style.css">
  <!-- Tag HTML yg memuat jquery, merupakan library JavaScript agar penulisan kode JavaScript lebih mudah -->
  <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

  <title>Hanugrah - Daftar</title>
  <script>
    // Function get IPK
    function getIPK(nim) {
      const ipk = document.getElement
      $.ajax({
        type: "GET",
        url: 'ipk.php',
        data: {
          nim: nim
        },
        success: function (response) {
          $("#ipk").val(response)
        }
      });
    }
  </script>
</head>

<body>

  <!-- Navbar mengambil dari framework bootstrap 5.3 -->
  <!--nav section start-->
  <nav class="navbar navbar-expand-lg navbar-light bg-black fixed-top">

    <!-- Menggunakan container agar elemen2 yg ada berada sesuai pada ukuran layar(responsif) -->
    <div class="container">
      <!-- setting gaya pada Logo Halaman Web (Beasiswa Pintar) -->
      <a class="navbar-brand font-weight-bold text-white" href="#">Beasiswa Pintar</a>
      <!-- Menggunakan button karena berfungsi sbg toggler/tombol pengontrol tampilan -->
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          <!-- elemen navigasi "Pilihan Beasiswa" -->
          <li class="nav-item" style="margin-left: 40px;">
            <a class="nav-link active text-white" aria-current="page" href="index.php">Pilihan Beasiswa</a>
          </li>
          <!-- elemen navigasi "Daftar" -->
          <li class="nav-item" style="margin-left: 40px;">
            <a class="nav-link text-white" href="daftar.php">Daftar</a>
          </li>
          <!-- elemen navigasi "Hasil" -->
          <li class="nav-item" style="margin-left: 40px;">
            <a class="nav-link text-white" href="hasil.php">Hasil</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--nav section end-->

  <!--jumbotron first-->
  <!--  -->
  <div class="jumbotron">
    <div class="container">
      <br><br><br>
      <h1 class="display-4">AYO PILIH BEASISWA <br> YANG SESUAI UNTUKMU!</h1>
      <p class="lead">Pastikan kamu sudah mengetahui berbagai jenis beasiswa yang disediakan oleh "Beasiswa Pintar".</p>
      <hr class="my-4">
      <p>Sudah siap? Ayo daftarkan dirimu pada formulir berikut</p>
    </div>
  </div>
  <br><br><br>
  <!--jumbotron end-->

  <!--form first-->
  <div class="container">
    <h1>Formulir Pendaftaran Beasiswa</h1>
    <br><br>
    <form id="registrationForm" method="post" action="upload.php" enctype="multipart/form-data">

      <!--input nim-->
      <div class="mb-3">
        <label class="form-label">NIM</label>
        <input type="text" class="form-control" onkeyup="getIPK(this.value)" id="nim" name="nama">
      </div>

      <!--input nama-->
      <div class="mb-3">
        <label class="form-label" for="namamhs">Nama</label>
        <input type="text" class="form-control" id="namamhs" name="namamhs">
      </div>

      <!--input email-->
      <div class="mb-3">
        <label class="form-label">Email</label>
        <input type="email" class="form-control" id="inputEmail" name="email">
      </div>

      <!--input no hp-->
      <div class="mb-3">
        <label class="form-label">No Handphone</label>
        <input type="tel" class="form-control" id="inputnohp" name="nohp" pattern="[0-9]*">
      </div>

      <!--input semester-->
      <div class="mb-3">
        <label class="form-label">Semester</label>
        <select class="form-select" id="semester" name="semester">
          <option value="" selected> Pilih Semester </option>
          <option value="1"> 1 </option>
          <option value="2"> 2 </option>
          <option value="3"> 3 </option>
          <option value="4"> 4 </option>
          <option value="5"> 5 </option>
          <option value="6"> 6 </option>
          <option value="7"> 7 </option>
          <option value="8"> 8 </option>
        </select>
      </div>

      <!--input ipk-->
      <div class="mb-3">
        <label class="form-label">IPK</label>
        <input type="number" class="form-control" id="ipk" name="inputipk" readonly>
      </div>

      <!--input pilih beasiswa-->
      <div class="mb-3">
        <label class="form-label">Beasiswa</label>
        <select class="form-select" id="beasiswa" name="beasiswa">
          <option value="" selected> Pilih Beasiswa </option>
          <option value="Beasiswa Akademik">1. Beasiswa Akademik</option>
          <option value="Beasiswa Non Akademik">2. Beasiswa Non Akademik</option>
        </select>
      </div>
      <br>

      <!--upload berkas-->
      <div class="mb-3">
        <label class="form-label">Upload Berkas Syarat</label>
        <input type="file" class="form-control" id="berkas" name="berkas" required>
        <div class="invalid-feedback">Example invalid form file feedback</div>
      </div>

      <!--button-->
      <br>
      <button type="submit" name="submit" id="submit" class="btn btn-primary" disabled>Daftar</button>
      <button type="reset" class="btn btn-danger">Batal</button>
      <br>
    </form>
  </div>
  <!--form end-->

  <!--CDN JS-->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"
    integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous">
    </script>
</body>

<script>
  let output = document.getElementById("nim"); //elemen id-nim diakses melalui variable output ini
  let button = document.querySelector("#submit"); //elemen id-submit dapat diakses melalui variable ini
  let berkas = document.querySelector("#berkas"); //menggunakan query selector karena memungkinkan penggunaan berbagai jenis selektor CSS.
  let beasiswa = document.querySelector("#beasiswa");

  button.disabled = true;
  berkas.disabled = true;
  beasiswa.disabled = true; //setting button kondisi menjadi disabled/
  output.addEventListener("change", stateHandle);
  // Ini menambahkan event listener pada elemen HTML dengan ID "output". Event listener ini akan dipanggil 
  // setiap kali nilai elemen tersebut berubah (event "change") dan akan menjalankan fungsi stateHandle. 
  // Fungsi stateHandle mungkin memiliki logika tertentu yang akan dieksekusi saat nilai elemen "output" berubah.

  function stateHandle() {
    if (document.getElementById("ipk").value < 3) {
      button.disabled = true; //button disabled
      berkas.disabled = true;
      beasiswa.disabled = true;

    } else {
      button.disabled = false; //button enabled
      berkas.disabled = false;
      beasiswa.disabled = false;
    }
  }
</script>

</html>